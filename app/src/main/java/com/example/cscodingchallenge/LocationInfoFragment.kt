package com.example.cscodingchallenge


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

class LocationInfoFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateUI(view)
    }

    private fun updateUI(view: View) {
        val model = activity?.run {
            ViewModelProviders.of(this)[LocationInfoViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        val poi = model.poi

        if (poi != null) {
            view.findViewById<TextView>(R.id.locationName).text = poi.name
            view.findViewById<TextView>(R.id.locationID).text = "ID: ${poi.placeId}"
            view.findViewById<TextView>(R.id.latLng).text = "${poi.latLng.latitude}, ${poi.latLng.longitude}"
        }
    }


}
