package com.example.cscodingchallenge


import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PointOfInterest


class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnPoiClickListener {

    private lateinit var map: GoogleMap
    private var currentPos = LatLng(52.520500, 13.405372)
    private var initialCall = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(mMap: GoogleMap) {
        map = mMap
        map.uiSettings.isZoomControlsEnabled = true
        map.setOnPoiClickListener(this)
        if (!initMapWithPermission()) {
            Log.d("Timur", "berlin")
            map.moveCamera(CameraUpdateFactory.newLatLng(currentPos))
        }
    }

    private fun initMapWithPermission(): Boolean {
        val permissionGranted = ActivityCompat.checkSelfPermission(
            context!!,
            android.Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

        if (initialCall && permissionGranted) {
            val mFusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(activity!!)

            mFusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->
                val result = task.result
                if (result != null) {
                    map.isMyLocationEnabled = true
                    currentPos = LatLng(result.latitude, result.longitude)
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPos, 12F))
                }
            }
        }
        return permissionGranted
    }

    override fun onPoiClick(poi: PointOfInterest) {
        val model = activity?.run {
            ViewModelProviders.of(this)[LocationInfoViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        model.poi = poi
        currentPos = map.cameraPosition.target
        initialCall = false
        findNavController().navigate(R.id.action_mapFragment_to_locationInfoFragment)
    }

}
