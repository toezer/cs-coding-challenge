package com.example.cscodingchallenge

import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.PointOfInterest

class LocationInfoViewModel: ViewModel() {
    var poi: PointOfInterest? = null
}